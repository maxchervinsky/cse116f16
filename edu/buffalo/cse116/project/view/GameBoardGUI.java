package edu.buffalo.cse116.project.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import edu.buffalo.cse116.project.model.ClueGame;
import edu.buffalo.cse116.project.model.Player;
import edu.buffalo.cse116.project.model.Tile;
import edu.buffalo.cse116.project.model.Weaponcards;

public class GameBoardGUI {

	public int row = 13;
	public int col = row;
	
	private JPanel _boardPanel;
	
	private JPanel _playerInfoPanel;
	
	private JFrame _window;
	
	private JPanel _leftPanel;
	
	private JPanel _rightPanel;
	
	private ClueGame _gb;
	
	private JTextPane _playerInfo;
	private JTextPane _playerInfo2;
	private JTextPane _playerInfo3;
	private JTextPane _playerInfo4;
	private JTextPane _playerInfo5;
	private JTextPane _playerInfo6;
	
	private JButton _endTurnButton;
	
	private JPanel _buttonPanel;	
	
	public GameBoardGUI(){
		
		JFrame board = new JFrame();
		board.setSize(1000, 1000);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(13, 13));
		

		for(int i = 0; i < row; i++){
			for(int j = 0; j<col; j++){
				JButton btn = new JButton(""+i+", "+j);
				btn.setSize(200,200);
				mainPanel.add(btn);
			}
		}
		
		board.setVisible(true);
		
		board.add(mainPanel);
	}
	
	public void run(){
		_window = new JFrame("CLUE!");
		
		_window.setSize(1440, 700);
		_window.setBackground(new Color(245, 245, 220));
		_window.getContentPane().setLayout(new GridLayout(1, 2));
		_leftPanel = new JPanel();
		_rightPanel = new JPanel();
		
	}
}
