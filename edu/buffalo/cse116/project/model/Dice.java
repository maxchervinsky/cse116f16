package edu.buffalo.cse116.project.model;

import java.awt.Font;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Dice {
	int _sum = 0;
	int _diceNumber;
	int _diceSides;
	JFrame diceframe; 
	JPanel dicepanel;
	JLabel dicelabel;
	public int _diceNum;
	public Dice() {
		_diceNumber = 1;
		_diceSides = 6;
		
		
	}
	public Dice(int number){
		_diceSides =6;
		_diceNumber = number;
	}
	
	public Dice(int number, int numberSides){
		_diceSides = numberSides;
		_diceNumber = number;
	}
	
	public int diceroll(){
		Random r = new Random();
		
		for(int i =0; i<_diceNumber;i++){
			_sum+=r.nextInt(_diceSides) + 1;
			
		} 
		return _sum;
	}
	public void GUI(){
		diceframe = new JFrame();
		diceframe.setVisible(true);
		diceframe.setResizable(false);
		dicepanel = new JPanel();
		dicelabel = new JLabel("Your roll is "+_sum);
		dicelabel.setFont(new Font("Papyrus", Font.ITALIC, 100));
		diceframe.add(dicepanel);
		dicepanel.add(dicelabel);
		dicepanel.setSize(300, 500);
	
	
	}
	
}
