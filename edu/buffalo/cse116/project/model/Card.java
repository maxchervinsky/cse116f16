package edu.buffalo.cse116.project.model;

public class Card {
	String _playerCard;
	String _roomCard;

	// public Card(String player){
	// _playerCard = player; //6cards
	// }
	// public Card(String room){
	// _roomCard = room;
	// }
	public Card() {

	}

	public int type = 0;

	public int value = 0;

	public String card = null;

	public static final int NUM_SUSPECTS = 6;

	public static final int NUM_ROOMS = 9;

	public static final int NUM_WEAPONS = 6;

	public static final int TOTAL = NUM_ROOMS + NUM_SUSPECTS + NUM_WEAPONS;

	
	
	
	public static final int TYPE_SUSPECT = 0;

	public static final int TYPE_WEAPON = 1;

	public static final int TYPE_ROOM = 2;

	public static final int ROOM_HALL = 0;

	public static final int ROOM_LOUNGE = 1;

	public static final int ROOM_DINING = 2;

	public static final int ROOM_KITCHEN = 3;

	public static final int ROOM_BALLROOM = 4;

	public static final int ROOM_CONSERVATORY = 5;

	public static final int ROOM_BILLIARD = 6;

	public static final int ROOM_STUDY = 7;

	public static final int ROOM_LIBRARY = 8;

//start of implementing roomcard description


	public static final int SUSPECT_Ms_Vivienne_Scarlet = 0;

	public static final int SUSPECT_Mrs_Blanche_White = 1;

	public static final int SUSPECT_Prof_Peter_Plum = 2;

	public static final int SUSPECT_Col_Michael_Mustard = 3;

	public static final int SUSPECT_Rev_Jonathan_Green = 4;

	public static final int SUSPECT_Mrs_Elizabeth_Peacock = 5;

	
	//start of implementing suspectcard description
	
	public static final int WEAPON_KNIFE = 0;

	public static final int WEAPON_ROPE = 1;

	public static final int WEAPON_REVOLVER = 2;

	public static final int WEAPON_WRENCH = 3;

	public static final int WEAPON_PIPE = 4;

	public static final int WEAPON_CANDLE = 5;

	//start of implementing weaponcard description
	
	
	public static final String Col_Michael_Mustard_card = "Colonel Col_Michael_Mustard: was an old friend of Mr. Boddy's uncle and a frequent guest at the Tudor Mansion.";

	public static final String Prof_Peter_Plum_card = "Professor Prof_Peter_Plum: A Professor in Middle Eastern history had many of his archaeological digs funded by Mr. Boddy's uncle.";

	public static final String Ms_Vivienne_Scarlet_card = "Miss Ms_Vivienne_Scarlett: An aspiring but not very talented actress. Has decided on a career change and is now setting her sights on rich old widowers, which is why she is at Mr.Boddy's dinner party.";

	public static final String Rev_Jonathan_Green_card = "Mr. Rev_Jonathan_Green: a trickster and conman has become acquainted with Mr. Boddy through his uncle, Sir Hugh Black.";

	public static final String Mrs_Elizabeth_Peacock_card = "Mrs. Mrs_Elizabeth_Peacock: is Miss. Ms_Vivienne_Scarlett's mother, a socialite and three time widow, she still dreams of a career on the stage.";

	public static final String Mrs_Blanche_White_card = "Mrs. Mrs_Blanche_White: Mr. Boddy's long-term housekeeper and cook.";

	public static final String[] cardriptions = { Ms_Vivienne_Scarlet_card, Mrs_Blanche_White_card, Prof_Peter_Plum_card, Col_Michael_Mustard_card, Rev_Jonathan_Green_card,
			Mrs_Elizabeth_Peacock_card };

	public static final Card Ms_Vivienne_Scarlet = new Card(TYPE_SUSPECT, SUSPECT_Ms_Vivienne_Scarlet);

	public static final Card Col_Michael_Mustard = new Card(TYPE_SUSPECT, SUSPECT_Col_Michael_Mustard);

	public static final Card Rev_Jonathan_Green = new Card(TYPE_SUSPECT, SUSPECT_Rev_Jonathan_Green);

	public static final Card Prof_Peter_Plum = new Card(TYPE_SUSPECT, SUSPECT_Prof_Peter_Plum);

	public static final Card Mrs_Elizabeth_Peacock = new Card(TYPE_SUSPECT, SUSPECT_Mrs_Elizabeth_Peacock);

	public static final Card Mrs_Blanche_White = new Card(TYPE_SUSPECT, SUSPECT_Mrs_Blanche_White);

	//Start of implementing peoplecard description
	
	public Card(int type, int value) {

		this.type = type;

		this.value = value;

		this.card = toString();

	}

	public int getType() {

		return type;

	}

	public void setType(int type) {

		this.type = type;

	}

	public int getValue() {

		return value;

	}

	public void setValue(int value) {

		this.value = value;

	}

	@Override

	public int hashCode() {

		return type + value;

	}

	@Override

	public boolean equals(Object obj) {

		if (obj instanceof Card) {

			Card Ca = (Card) obj;

			return (Ca.type == this.type && Ca.value == this.value);

		} else {

			return false;

		}

	}

	public String getcardription() {

		return cardriptions[value];

	}

	public String toString() {

		String card = null;

		switch (type) {

		case TYPE_SUSPECT:

			switch (value) {

			case SUSPECT_Ms_Vivienne_Scarlet:

				card = "Miss Ms_Vivienne_Scarlet";

				break;

			case SUSPECT_Prof_Peter_Plum:

				card = "Professor Prof_Peter_Plum";

				break;

			case SUSPECT_Mrs_Blanche_White:

				card = "Mrs. Mrs_Blanche_White";

				break;

			case SUSPECT_Col_Michael_Mustard:

				card = "Colonel Col_Michael_Mustard";

				break;

			case SUSPECT_Rev_Jonathan_Green:

				card = "Mr. Rev_Jonathan_Green";

				break;

			case SUSPECT_Mrs_Elizabeth_Peacock:

				card = "Mrs. Mrs_Elizabeth_Peacock";

				break;

			}

			break;

		case TYPE_ROOM:

			switch (value) {

			case ROOM_HALL:

				card = "Hall";

				break;

			case ROOM_LOUNGE:

				card = "Lounge";

				break;

			case ROOM_DINING:

				card = "Dining Room";

				break;

			case ROOM_KITCHEN:

				card = "Kitchen";

				break;

			case ROOM_BALLROOM:

				card = "Ballroom";

				break;

			case ROOM_CONSERVATORY:

				card = "Conservatory";

				break;

			case ROOM_BILLIARD:

				card = "Billiard Room";

				break;

			case ROOM_STUDY:

				card = "Study";

				break;

			case ROOM_LIBRARY:

				card = "Library";

				break;

			}

			break;

		case TYPE_WEAPON:

			switch (value) {

			case WEAPON_KNIFE:

				card = "Knife";

				break;

			case WEAPON_ROPE:

				card = "Rope";

				break;

			case WEAPON_REVOLVER:

				card = "Revolver";

				break;

			case WEAPON_WRENCH:

				card = "Wrench";

				break;

			case WEAPON_PIPE:

				card = "Pipe";

				break;

			case WEAPON_CANDLE:

				card = "Candlestick";

				break;

			}

			break;

		}

		return card;
	}
}
