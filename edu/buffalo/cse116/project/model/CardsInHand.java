
package edu.buffalo.cse116.project.model;

import java.awt.Dialog;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class CardsInHand extends javax.swing.JDialog {
String _dialog;
private javax.swing.JPanel panel;
private javax.swing.JLabel panelLabel;
private javax.swing.JPanel label2;
private javax.swing.JTextArea JTextSpot1;
private javax.swing.JButton yesButton;
public void CardsInhand(String dialog) {
this._dialog = dialog;
inCardComponents(dialog);
}
private void inCardComponents(String text){
java.awt.GridBagConstraints gridBagConstraints;

//The Line below is supposed to set the location in the middle, if this isn�t the middle change the coordinates to the middle
setUndecorated(true);

JPanel panel = new javax.swing.JPanel();
JPanel panel2 = new javax.swing.JPanel();
JTextArea JTextSpot1 = new javax.swing.JTextArea();
JButton yesButton = new javax.swing.JButton();
JLabel panelLabel = new javax.swing.JLabel();

panel.setMinimumSize(new java.awt.Dimension(1,1));
panel.setLayout(new java.awt.GridBagLayout());

panel2.setBackground(java.awt.SystemColor.info);
//Can set whatever dimension you want this is just a guess)
panel2.setPreferredSize(new java.awt.Dimension(340,230));
JTextSpot1.setEditable(false);
JTextSpot1.setHighlighter(null);

JTextSpot1.setRows(5);
JTextSpot1.setText(text);

yesButton.setText(text);

panel.add(panel2, new java.awt.GridBagConstraints());

gridBagConstraints = new java.awt.GridBagConstraints();
gridBagConstraints.gridx = 0;
gridBagConstraints.gridy = 0;
gridBagConstraints.fill = java.awt. GridBagConstraints.BOTH;
gridBagConstraints.weightx = 1.0;
gridBagConstraints.weighty = 1.0;
panel.add(panelLabel, gridBagConstraints);

getContentPane().setLayout(getLayout());

pack();
}

private void yesButtonActionPerformed(ActionEvent event){
dispose();
}}

