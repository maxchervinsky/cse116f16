package edu.buffalo.cse116;

import java.awt.Color;

public class Player {
	public int _playerNum;
	public Color _playerColor;
	public boolean _isInRoom;
	public Integer[] _playerLocation;

	public Player(int number, Color color, Integer[] location) {
		this._playerNum = number;
		this._playerColor = color;
		this._playerLocation = location;
		this._isInRoom = false;
	}

}
