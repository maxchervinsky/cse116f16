package edu.buffalo.cse116;

import javax.swing.JFrame;
import javax.swing.JPanel;

import edu.buffalo.cse116.project.model.GuiApp1;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GuiAppl {

public static void main(String[] args) {

new GuiApp1();
}

public GuiAppl()
{
JFrame guiFrame = new JFrame();

//make sure the program exits when the frame closes
guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
guiFrame.setTitle("Players");
guiFrame.setSize(300,250);

//This will center the JFrame in the middle of the screen
guiFrame.setLocationRelativeTo(null);

//Options for the JComboBox 
String[] playerOptions = {"Miss Scarlet", "Professor Plumb", "Mr. Green"
,"Mrs. White", "Mrs. Peacock", "Col. Mustard"};

//Options for the JList
String[] weaponOptions = {"Poison", "Knife", "Revolver", "Rope"
, "Candle Stick"};

//The first JPanel contains a JLabel and JCombobox
final JPanel comboPanel = new JPanel();
JLabel playerLbl = new JLabel("Players");
JComboBox weapon = new JComboBox(weaponOptions);

comboPanel.add(playerLbl);
comboPanel.add(weapon);

//Create the second JPanel. Add a JLabel and JList and
//make use the JPanel is not visible.
final JPanel listPanel = new JPanel();
listPanel.setVisible(false);
JLabel players = new JLabel("Players");
JList weapons = new JList("Weapons");
players.setLayoutOrientation(JList.HORIZONTAL_WRAP);

listPanel.add(players);
listPanel.add(weapons);

JButton weaponBut = new JButton( "Player or Weapon");


playersBut.addActionListener(new ActionListener()
{
@Override
public void actionPerformed(ActionEvent event)
{

listPanel.setVisible(!listPanel.isVisible());
comboPanel.setVisible(!comboPanel.isVisible());

}
});

//The JFrame uses the BorderLayout layout manager.
//Put the two JPanels and JButton in different areas.
guiFrame.add(comboPanel, BorderLayout.NORTH);
guiFrame.add(listPanel, BorderLayout.CENTER);
guiFrame.add(vegFruitBut,BorderLayout.SOUTH);

//make sure the JFrame is visible
guiFrame.setVisible(true);
}

}



