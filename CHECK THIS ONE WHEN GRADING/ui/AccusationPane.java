package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextField;

public class AccusationPane extends JTextField {
	UI _ui;
	int _state = 0;
	ArrayList<String> _inputs = new ArrayList<String>();
	
	AccusationPane _self = this;
	
	public AccusationPane (UI ui){
		_ui = ui;
		this.setEnabled(false);
		
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String input = e.getActionCommand();
				
				switch (_state){
				
					case 0:
						_state++;
						setText("");
						_inputs.add(input);
						_ui.displayInstruction("WRITE PERSON THAT YOU ARE GOING TO ACCUSE");
						break;
						
					case 1:
						_state++;
						_self.setText("");
						_inputs.add(input);
						_ui.displayInstruction("WRITE WHICH WEAPON YOU ARE GOING TO ACCUS");
						break;
						
					case 2:
						_state = 0;
						_self.setText("");
						_inputs.add(input);
						_self.setEnabled(false);
						_ui.passSuggestion(_inputs);
						break;
				}
				
			}
			
		});
	}
	public void grabAccusationInput() {
		_state = 0;
		_inputs = new ArrayList<String>();
		this.setEnabled(true);
		_ui.displayInstruction("PLEASE WRITE THE WEAPON ROOM");
	}

}
